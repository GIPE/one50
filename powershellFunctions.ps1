
# Execute a FLOW command in the php docker container
function flow([string] $cmd) {
	docker exec -it one50_php ./flow $cmd
}

# Execute a COMPOSER command in the corresponding docker container
function composer([string] $cmd) {
    docker-compose run --rm -w /app composer composer $cmd
}