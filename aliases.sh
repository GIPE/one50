# the script is expected to be in [Project Root Folder]/Docker
APPLICATION_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

alias flow='docker exec -it one50_php ./flow'

alias composer='docker-compose run --rm -w /app composer composer'