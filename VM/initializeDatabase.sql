# If database/user exist: drop them
DROP DATABASE IF EXISTS one50;
DROP DATABASE IF EXISTS one50;

# Create database and user
CREATE DATABASE one50;
CREATE USER 'one50'@'localhost' IDENTIFIED BY 'm150';

# Grant user 'one50' access to database 'one50'
GRANT ALL PRIVILEGES ON 'one50'.* TO 'one50'@'localhost';