# Installation for Docker
This document contains instructions for installing the One50 e-shop on your host machine using _Docker_. Please make sure to read the general [README](./README.md) document first!

## Docker
Docker is used for running the whole application on any operating system. The only requirement for running the One50 e-shop locally is a working and up-to-date installation of Docker (including Docker Compose).

_Please note: Although it's not impossible to run Docker in a virtual machine, it's highly recommended to **not** do so! Do install Docker on your host machine. If that's not possible for any reason - use the provided virtual machine._

## Installation
### Disclaimer
This installation process was developed and tested on macOS. It was verified on macOS and Linux. At the time of this writing, it is _not_ tested on Windows. You might encounter problems and errors while doing so.

Please understand, that at this time your teacher won't be able to individually support you with this installation process. If something does not work, feel free to use the provided virtual machine as described in the [README](./README.md) document.

If you manage to run this application on docker - especially on Windows - any kind of feedback is highly welcome!  

### Prerequisites
Before you start with the installation process as described bellow, please make sure to meet all prerequisites:

#### Git
You'll need Git for cloning the initial project. While working on the project you'll constantly share and synchronize the source code with your team mates using Git.
If you don't already have Git installed on your machine, head over to [https://git-scm.com/downloads](https://git-scm.com/downloads) for downloading Git.

#### Gitlab
You should have access to Gitlab (which most likely is the case while you're reading this). Furthermore you should create an _Access Token_ on Gitlab.

Follow the instructions on [https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) to create an Access Token. While you won't need activated all scopes, it's safe to check all available options.

#### Docker
You'll further need Docker to run the PHP application on a local webserver instance. Docker will also provide you with a container for the local MySQL database.
Go to [https://store.docker.com/](https://store.docker.com/) for downloading the _Community Edition (CE)_ of Docker. After downloading you should run the installation tool. This will install _Docker_ along with _Docker Compose_ and some other useful tools. 

**Write down the access token after generation.** You won't be able to recover this token. If you lose the token, you'll have to revoke it and generate a new one! 

### Installation
Follow these steps to install and run the One50 shop locally. All commands are to be executed in a terminal (Linux/macOS) or powershell (Windows) session respectively.

#### Clone repository
Clone this repository on your machine. Grab the SSH version of the repository url from Gitlab and enter in a terminal/powershell session. Make sure to actually grab the correct url - don't copy the below stated url!

`git clone [git@gitlab.com:gibz-informatik/M150/[schoolyear]/[class]/[team]/one50.git]`

#### Change directory
The above command cloned the remote repository locally on your machine. The project files are located in a directory named after the git repository: `one50`. You should now change into that directory for proceeding with the installation...

`cd one50`

#### Source aliases / functions
Since we'll be working with docker and multiple containers, sometimes it can be quite tiresome to remember and type the same commands again and again. Therefore there are two files called `aliases.sh` and `powershellFunctions.ps1` to provide you with useful shortcuts regarding some command line tools.

You'll need only one of these to files - depending on your operating systems:
- On Linux and macOS you'll use `aliases.sh`
- On Windows you'll use `powershellFunctions.ps1`

**Always make sure to source this file before executing any of the aliased commands.** Otherwise you might get unexpected results.

- Linux / macOS: `. aliases.sh`
- Windows: `. powershellFunctions.ps1`

If you get an error message regarding a policy error in powershell, additionally fire the following command before sourcing the `powershellFunctions.ps1` again: `Set-ExecutionPolicy RemoteSigned -Scope CurrentUser`

_Hint: From now on, **both** files are both simply referred to as **aliases**._ 

#### Set your Access Token
For checking out your private _Two Factor Authentication_ package you'll need to provide your Gitlab access token to Git. Everything in the project setup is prepared for that. The only thing you need to do is to store your access token in an environment variable named `GITLAB_ACCESS_TOKEN`.

`echo "GITLAB_ACCESS_TOKEN=[ YOUR TOKEN HERE ] >> .env`

#### Install composer packages
[Composer](https://getcomposer.org) is a dependency manager for PHP projects. It is used for resolving and downloading all required dependencies for this project. The command runs inside a dedicated docker container. This way you won't have to install composer locally. Just make sure to source the aliases in every terminal/powershell session in which you execute composer commands.

`comopser install`

This might take a few minutes...

#### Run docker containers
Now it's time to fire up docker (more precise: several docker containers). Just issue the following command to run all services declared in `docker-compose.yaml` as background proceses:

`docker-compose up -d`

#### Initialize database
At this point you'll have running a MySQL database container (actually it's MariaDB which is very simmilar to MySQL). Although, there are no tables yet. As you've read earlier, all tables will be generated according to data models declared in PHP code. The command to initialize all database tables is:

`flow doctrine:create`

This is a flow command which is provided by the _Flow_ framework. Just as before with `composer` commands, always make sure to source the aliases before invoking any `flow` commands!

#### Check it out!
Fire up your browser and type [http://localhost](http://localhost) to checkout the (empty) One50 shop!

The only thing you might actually do now is to sign up yourself as customer of your own shop :-) We will although kickstart some fruits and veggies at the next step...

#### Kickstart some products
Type the following command to automatically import some (predefined) products:

`flow shopdata:import`

Reload the shop in your browser and shop through some tasty and healthy food!

#### Initialize customer user
Since you probably won't have signed up a few steps before, you might do that now - in a technologist way :-)

`flow user:createuser`

After entering this command you'll be asked to provide username, passwort and some other information for creating a user. Of course you might repeat this command anytime.

#### Initialize admin user
Just as before, we'll initialize another user. This time the user to be created will be no customer but a almighty admin.

`flow user:createadmin`

Again, provide all required information for the admin user to be created...